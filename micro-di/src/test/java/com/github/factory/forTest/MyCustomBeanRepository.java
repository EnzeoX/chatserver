package com.github.factory.forTest;

import com.github.annotations.CustomRepository;
import com.github.annotations.InjectValue;

@CustomRepository
public class MyCustomBeanRepository {

    @InjectValue(name = "string")
    private final String string = new String("lol");

    public String getString() {
        return string;
    }
}
