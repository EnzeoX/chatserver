package com.github.factory;

import com.github.factory.forTest.MyCustomBeanRepository;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class CustomBeanFactoryTest {

    @Test
    public void registration() {
        Map<String, Object> properties = new HashMap<>();
        properties.put("string", "NEWONE");
        CustomBeanFactory factory = CustomBeanFactory.getInstance();
        factory.setProperties(properties);
        factory.registration(FirstBean.class);
        factory.wireObjects();
        MyCustomBeanRepository repo = factory.instance(MyCustomBeanRepository.class);
        System.out.println(repo);
    }

}