package com.github.utlis;

import com.github.annotations.InjectValue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.*;

public class ReflectionUtils {

    public static List<Class<?>> findFinalFields(Class<?> clz) {
        List<Class<?>> result = new ArrayList<>();
        Field[] fields = clz.getDeclaredFields();
        for (Field field : fields) {
            if (Modifier.isFinal(field.getModifiers())) {
                result.add(field.getType());
            }
        }
        return result;
    }

    public static Object newInstanceWithoutParams(Class<?> clz) {
        try {
            return clz.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException("Cannot make class instance : " + clz.getName());
        }
    }

    public static boolean hasProperties(Class<?> clz) {
        return Arrays.stream(clz.getDeclaredFields())
                .anyMatch(f -> f.isAnnotationPresent(InjectValue.class));
    }

    public static void injectProperties(Object obj, Class<?> clz, Map<String, Object> properties) {
        Field[] fields = clz.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(Boolean.TRUE);
                if (field.isAnnotationPresent(InjectValue.class)) {
                    String key = field.getAnnotation(InjectValue.class).name();
                    field.set(obj, properties.get(key));
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static List<Class<?>> getClasses(String packageName) {
        List<Class<?>> classes = new ArrayList<>();
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            assert classLoader != null;
            String path = packageName.replace('.', '/');
            Enumeration<URL> resources = classLoader.getResources(path);
            List<File> dirs = new ArrayList<>();
            while (resources.hasMoreElements()) {
                URL resource = resources.nextElement();
                dirs.add(new File(resource.getFile()));
            }
            for (File directory : dirs) {
                classes.addAll(findClasses(directory, packageName));
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return classes;
    }

    private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class<?>> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

}
