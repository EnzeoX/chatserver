package com.github.factory;

import com.github.annotations.*;
import com.github.utlis.ReflectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Rules:
 * -injects only by final fields.
 * -inject only by constructor.
 * -add config value by annotation @link @InjectValue.
 * -
 **/

public class CustomBeanFactory {

    public static final CustomBeanFactory instance = new CustomBeanFactory();

    private Map<String, Object> properties = new HashMap<>();

    private Map<Class<?>, List<Class<?>>> adjacency = new HashMap<>();

    private Map<Class<?>, Object> container = new HashMap<>();

    private CustomBeanFactory() {
    }

    public static CustomBeanFactory getInstance() {
        return instance;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public void registration(Class<?> mainClass) {
        List<Class<?>> classes = ReflectionUtils.getClasses(mainClass.getPackageName());
        this.adjacency = classes.stream().filter(clz -> clz.isAnnotationPresent(CustomService.class) ||
                clz.isAnnotationPresent(CustomController.class) ||
                clz.isAnnotationPresent(CustomComponent.class) ||
                clz.isAnnotationPresent(CustomRepository.class) ||
                clz.isAnnotationPresent(CustomConfiguration.class)).collect(Collectors.toMap(Function.identity(), v -> new ArrayList<Class<?>>()));
        System.out.println(this.adjacency);
    }

    public void buildDependencies() {
        Set<Class<?>> keys = this.adjacency.keySet();
        for (Class<?> source : keys) {
            List<Class<?>> sourceClasses = ReflectionUtils.findFinalFields(source);
            if (this.adjacency.containsKey(source)) {
                this.adjacency.put(source, sourceClasses);
            }
            for(Class<?> destination : sourceClasses) {
                List<Class<?>> destinationClasses = ReflectionUtils.findFinalFields(destination);
                if(this.adjacency.containsKey(destination)) {
                    this.adjacency.put(destination, destinationClasses);
                }
            }
        }
    }

    public void wireObjects() {
        this.buildDependencies();
        List<Class<?>> unconnected = this.findAllUnconnected();
        for(Class<?> clz : unconnected) {
            Object obj = this.unconnectedObject(clz);
            this.container.put(clz, obj);
        }

        // TODO: 24.05.2021 Use unconnected and init next 

        // TODO: 24.05.2021 All classes which not wired
    }

    public List<Class<?>> findAllUnconnected() {
        return this.adjacency.keySet().stream()
                .filter(c -> this.adjacency.get(c).isEmpty())
                .collect(Collectors.toList());
    }

    private Object unconnectedObject(Class<?> clz) {
        Object object = ReflectionUtils.newInstanceWithoutParams(clz);
        if(ReflectionUtils.hasProperties(clz)) {
            ReflectionUtils.injectProperties(object, clz, this.properties);
        }
        return object;
    }

    @SuppressWarnings("unchecked")
    public <T> T instance(Class<?> clz) {
        return (T) this.container.get(clz);
    }
}
