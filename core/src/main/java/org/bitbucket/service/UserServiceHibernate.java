package org.bitbucket.service;

import org.bitbucket.dto.UserAuthorizationDto;
import org.bitbucket.dto.UserRegistrationDto;
import org.bitbucket.entity.User;
import org.bitbucket.repository.UsersRepositoryHibernate;
import org.bitbucket.utils.SaltProvider;
import org.bitbucket.utils.TransferObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserServiceHibernate implements IUserServiceHibernate{

    private static final Logger logger = LoggerFactory.getLogger(UserServiceHibernate.class);

    private final UsersRepositoryHibernate usersRepositoryHibernate;

    public UserServiceHibernate(UsersRepositoryHibernate usersRepositoryHibernate) {
        this.usersRepositoryHibernate = usersRepositoryHibernate;
    }

    @Override
    public User findById(long id) {
        return null;
    }

    @Override
    public User findByAuth(UserAuthorizationDto auth) {
        User result = (User) this.usersRepositoryHibernate.findByLogin(auth.getLogin());
        String checkPass = SaltProvider.encrypt(auth.getPassword() + result.getSalt());
        if(!result.getHashcode().equals(checkPass)) {
            return null;
        }
        return result;
    }

    @Override
    public User insert(UserRegistrationDto reg) {
        return (User)this.usersRepositoryHibernate.insert(TransferObject.toUser(reg));
    }

    @Override
    public void delete(UserRegistrationDto reg) {
        this.usersRepositoryHibernate.delete(reg);

    }

    @Override
    public void update(UserRegistrationDto reg) {
        this.usersRepositoryHibernate.update(reg);
    }
}
