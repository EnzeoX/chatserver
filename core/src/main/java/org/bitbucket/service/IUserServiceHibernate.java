package org.bitbucket.service;

import org.bitbucket.dto.UserAuthorizationDto;
import org.bitbucket.dto.UserRegistrationDto;
import org.bitbucket.entity.User;

public interface IUserServiceHibernate {

    User findById(long id);

    User findByAuth(UserAuthorizationDto auth);

    User insert(UserRegistrationDto reg);

    void delete(UserRegistrationDto reg);

    void update(UserRegistrationDto reg);

}
