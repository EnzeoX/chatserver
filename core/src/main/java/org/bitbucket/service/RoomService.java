package org.bitbucket.service;

import org.bitbucket.entity.Room;
import org.bitbucket.payload.Message;
import org.bitbucket.repository.RoomRepository;

public class RoomService implements IRoomService {

    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public void createRoom(Room room) {
        String roomName = "test_room";
        this.roomRepository.createRoom(roomName);
    }

    @Override
    public void findRoom(Room room) {

    }

    @Override
    public Message addMessage(String message, long userId) {
        return null;
    }

    @Override
    public void deleteMessage() {

    }

    @Override
    public void deleteRoom(long roomId) {

    }
}
