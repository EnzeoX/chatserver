package org.bitbucket.service;

import org.bitbucket.payload.Message;

public interface IMessageService {

    void createMessageRoom();

    void findRoom();

    Message addMessage(String message, long userId);

    void deleteMessage();

    void deleteRoom(long roomId);
}
