package org.bitbucket.service;


import org.bitbucket.entity.Room;
import org.bitbucket.payload.Message;

public interface IRoomService {

    void createRoom(Room room);

    void findRoom(Room room);

    Message addMessage(String message, long userId);

    void deleteMessage();

    void deleteRoom(long roomId);
}
