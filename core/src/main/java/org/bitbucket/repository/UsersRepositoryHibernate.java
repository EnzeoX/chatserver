package org.bitbucket.repository;

import org.bitbucket.entity.User;
import org.bitbucket.utils.HibernateSessionFactory;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class UsersRepositoryHibernate<T> implements IUserRepositoryHibernate {

    @Override
    public Object findById() {
        return null;
    }

    @Override
    public Object findByLogin(Object login) {
        Session session = HibernateSessionFactory.getSession();
        System.out.println(login);
        Query query = session.createQuery("From User where login=:login")
                .setParameter("login", login);
        System.out.println(query);
        return query.getSingleResult();
    }

    @Override
    public Object findByEmail() {
        return null;
    }

    @Override
    public Object insert(Object entity) {
        Session session = HibernateSessionFactory.getSession();
        session.save(entity);
        return entity;
    }

    @Override
    public Object update(Object entity) {
        return null;
    }

    @Override
    public void delete(Object entity) {

    }

    @Override
    public void deleteAll() {

    }
}
