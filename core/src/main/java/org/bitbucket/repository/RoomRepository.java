package org.bitbucket.repository;

import org.bitbucket.entity.Room;
import org.bitbucket.micro.orm.CustomJdbcTemplate;
import org.bitbucket.utils.RowMappers;

public class RoomRepository implements IRoomRepository{

    private final CustomJdbcTemplate jdbcTemplate;

    public RoomRepository(CustomJdbcTemplate customJdbcTemplate) {
        this.jdbcTemplate = customJdbcTemplate;
    }

    public Room findRoom(Room room) {
        return jdbcTemplate.findBy(
                "SELECT * FROM rooms WHERE id = ?",
                RowMappers.getCustomRoomMapper(),
                room.getId()
        );
    }

    public void createRoom(String roomName) {

        jdbcTemplate.create("CREATE TABLE ? (roomId serial constraint bigint primary key, messages text, users bigint");
    }

    public void addMessage() {

    }

}
