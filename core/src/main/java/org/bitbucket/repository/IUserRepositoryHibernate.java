package org.bitbucket.repository;

import org.bitbucket.dto.UserAuthorizationDto;
import org.bitbucket.dto.UserRegistrationDto;
import org.bitbucket.entity.User;

import java.util.Collection;

public interface IUserRepositoryHibernate<T> {

    T findById();

    T findByLogin(T login);

    T findByEmail();

    T insert(T entity);

    T update(T entity);

    void delete(T entity);

    void deleteAll();
}
