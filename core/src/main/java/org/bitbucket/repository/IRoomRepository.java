package org.bitbucket.repository;

import org.bitbucket.entity.Room;
import org.bitbucket.payload.Message;

public interface IRoomRepository {

    public interface IRoomService {

        void createRoom(Room room);

        Room findRoom(Room room);

        Message addMessage(String message, long userId);

        void deleteMessage();

        void deleteRoom(long roomId);
    }

}
