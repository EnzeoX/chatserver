package org.bitbucket.entity;

import org.bitbucket.payload.Message;

import javax.persistence.*;
import javax.websocket.Session;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private List<Message> history;

    @ManyToOne
    private List<User> members;

    private List<Session> sessions;

    public Room() {
    }

    public Room(String name) {
        this.name = name;
        this.history = new LinkedList<>();
        this.members = new ArrayList<>();
        this.sessions = new ArrayList<>();
    }

    public Room(String name, Long id) {
        this.name = name;
        this.history = new LinkedList<>();
        this.members = new ArrayList<>();
        this.sessions = new ArrayList<>();
        this.id = id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }

    public void addMember(User user) {
        this.members.add(user);
    }

    public String getName() {
        return name;
    }

    public List<Message> getHistory() {
        return history;
    }

    public List<User> getMembers() {
        return members;
    }

    public List<Session> getSessions() {
        return sessions;
    }
}
