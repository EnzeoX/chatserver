package org.bitbucket.config;

import org.bitbucket.controllers.UserControllers;
import org.bitbucket.controllers.UserControllersHibernate;
import org.bitbucket.utils.HibernateSessionFactory;

public class ControllerConfig {


    public static UserControllers usersController(){
        return new UserControllers(DatabaseConfig.getUsersService());
    }

    public static UserControllersHibernate userControllersHibernate() {
        return new UserControllersHibernate(DatabaseConfig.getUsersServiceHibernate());
    }

}
