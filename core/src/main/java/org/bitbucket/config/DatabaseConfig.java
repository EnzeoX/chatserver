package org.bitbucket.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bitbucket.micro.orm.CustomJdbcTemplate;
import org.bitbucket.repository.UsersRepository;
import org.bitbucket.repository.UsersRepositoryHibernate;
import org.bitbucket.service.UserServiceHibernate;
import org.bitbucket.service.UsersService;

import javax.sql.DataSource;

public class DatabaseConfig {

    private static final String HIKARI_PROPERTY_PATH = "core/src/main/resources/hikariEnzeo.properties";

    public static DataSource getHikariDS() {
        HikariConfig config = new HikariConfig(HIKARI_PROPERTY_PATH);
        return new HikariDataSource(config);
    }

    public static UsersRepository getUsersRepository() {
        return new UsersRepository(new CustomJdbcTemplate(getHikariDS()));
    }

    public static UsersService getUsersService() {
        return new UsersService(getUsersRepository());
    }

    public static UsersRepositoryHibernate getUsersRepositoryHibernate() {
        return new UsersRepositoryHibernate();
    }

    public static UserServiceHibernate getUsersServiceHibernate() {
        return new UserServiceHibernate(getUsersRepositoryHibernate());
    }

}
