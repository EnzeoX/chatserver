package org.bitbucket.controllers;

import org.bitbucket.dto.UserAuthorizationDto;
import org.bitbucket.dto.UserRegistrationDto;
import org.bitbucket.entity.User;
import org.bitbucket.payload.Token;
import org.bitbucket.service.UserServiceHibernate;
import org.bitbucket.utils.SaltProvider;
import org.bitbucket.utils.TokenProvider;

public class UserControllersHibernate implements IUserControllers{

    private final UserServiceHibernate serviceHibernate;

    public UserControllersHibernate(UserServiceHibernate repositoryHibernate) {
        this.serviceHibernate = repositoryHibernate;
    }

    @Override
    public String authorize(UserAuthorizationDto payload) {
        User user = this.serviceHibernate.findByAuth(payload);
        return TokenProvider.encode(new Token(user));
    }

    @Override
    public void registration(UserRegistrationDto payload) {
//        if (this.repositoryHibernate.findByAuthDto(new UserAuthorizationDto(userRegistrationDto)) != null) {
//            throw new UserAlreadyExistException();
//        }
        payload.setSalt(SaltProvider.getRandomSalt());
        String hashCode = payload.getPassword() + payload.getSalt();
        payload.setHashCode(SaltProvider.encrypt(hashCode));
        this.serviceHibernate.insert(payload);
    }
}
