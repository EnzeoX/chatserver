package org.bitbucket.utils;

import org.bitbucket.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class HibernateSessionFactory {

    private static final Logger logger = LoggerFactory.getLogger(HibernateSessionFactory.class);

    private static SessionFactory sessionFactory;

    public HibernateSessionFactory() {
    }

    public static Session getSession() {
        System.out.println("GET SESSION");
        Session session = getSessionFactory().openSession();
        System.out.println("SESSION GET");
        return getSessionFactory().openSession();
    }

    public static void closeSession() {
        getSessionFactory().close();
    }

    private static SessionFactory getSessionFactory() {
        System.out.println("IN SESSION FACTORY METHOD");
        if (sessionFactory == null) {
            System.out.println("SESSION FACTORY NULL");
            try {
                System.out.println("CONFIGURING SESSION");
                Configuration configuration = new Configuration();
                System.out.println("SEARCHING FOR PROPERTIES");
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
                System.out.println("SEARCHING USER CLASS");
                configuration.addAnnotatedClass(User.class);
                System.out.println("CLASS USER FOUND");
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                logger.error(e.getMessage());
                throw new RuntimeException("There is issue in hibernate util");
            }
        }
        return sessionFactory;
    }

    private static List<Class<?>> getEntityClassesFromPackage(String packageName) throws ClassNotFoundException, URISyntaxException {
        List<String> classNames = getClassNamesFromPackage(packageName);
        List<Class<?>> classes = new ArrayList<Class<?>>();
        for (String className : classNames) {
            Class<?> cls = Class.forName(packageName + "." + className);
            Annotation[] annotations = cls.getAnnotations();

            for (Annotation annotation : annotations) {
                System.out.println(cls.getCanonicalName() + ": " + annotation.toString());
                if (annotation instanceof javax.persistence.Entity) {
                    classes.add(cls);
                }
            }
        }

        return classes;
    }

    private static List<String> getClassNamesFromPackage(String packageName) throws URISyntaxException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        List<String> names = new ArrayList<String>();

        packageName = packageName.replace(".", "/");
        URL packageURL = classLoader.getResource(packageName);

        URI uri = new URI(packageURL.toString());
        File folder = new File(uri.getPath());
        File[] files = folder.listFiles();
        for (File file : files) {
            String name = file.getName();
            name = name.substring(0, name.lastIndexOf('.'));  // remove ".class"
            names.add(name);
        }

        return names;
    }
}
