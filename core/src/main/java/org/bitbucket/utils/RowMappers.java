package org.bitbucket.utils;

import org.bitbucket.dto.UserAuthorizationDto;
import org.bitbucket.dto.UserRegistrationDto;
import org.bitbucket.entity.Room;
import org.bitbucket.entity.User;
import org.bitbucket.micro.orm.CustomRowMapper;

public class RowMappers {

    private static CustomRowMapper<UserAuthorizationDto> customRowMapperAuth;

    private static CustomRowMapper<User> customRowMapperUser;

    private static CustomRowMapper<UserRegistrationDto> customRowMapperReg;

    private static CustomRowMapper<Room> customRoomMapper;

    public static CustomRowMapper<UserAuthorizationDto> getCustomRowMapperAuthDto() {
        if (customRowMapperAuth == null) {
            customRowMapperAuth = rs -> new UserAuthorizationDto(
                    rs.getString("login"),
                    rs.getString("password"));
        }
        return customRowMapperAuth;
    }

    public static CustomRowMapper<User> getCustomRowMapperUser() {
        if (customRowMapperUser == null) {
            customRowMapperUser = rs -> new User(
                    rs.getInt("id"),
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getString("email"),
                    rs.getString("login"),
                    rs.getString("nickname"),
                    rs.getString("password"),
                    rs.getString("phone_number")
            );
        }
        return customRowMapperUser;
    }

    public static CustomRowMapper<UserRegistrationDto> getCustomRowMapperReg() {
        if (customRowMapperReg == null) {
            customRowMapperReg = rs -> new UserRegistrationDto(
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("login"),
                    rs.getString("nickname"),
                    rs.getString("password"),
                    rs.getString("confirmPassword"),
                    rs.getString("email"),
                    rs.getString("phone")
            );
        }
        return customRowMapperReg;
    }

    public static CustomRowMapper<Room> getCustomRoomMapper(){
        if (customRoomMapper == null) {
            customRoomMapper = rs -> new Room(
                    rs.getString("name"),
                    Long.parseLong(rs.getString("id"))
            );
        }
        return customRoomMapper;
    }
}
