package org.bitbucket.utils;

import org.bitbucket.dto.UserRegistrationDto;
import org.bitbucket.entity.User;

import java.sql.Date;

public class TransferObject {

    public static User toUser(UserRegistrationDto data) {
        return new User(1L,
                data.getFirstName(),
                data.getLastName(),
                data.getEmail(),
                data.getLogin(),
                data.getPassword(),
                data.getPhone(),
                data.getNickname(),
                "role",
                new Date(System.currentTimeMillis()),
                data.getSalt(),
                data.getHashCode()
        );
    }
}
